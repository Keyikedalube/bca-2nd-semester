/*
 * Write a C Program to count the number of elements in a linked list.
 */

#include <stdio.h>
#include <stdlib.h>

typedef struct node_properties {
    int data;
    struct node_properties *next;
} node;

node *head = NULL;
node *create = NULL;
node *current = NULL;

int main()
{
    // create a random size list
	int size = random() % 10;
    for (int i = 0; i < size; i++) {
        create = malloc(sizeof(node));
        if (create == NULL) {
            puts("Memory allocation failed!");
            exit(1);
        } else {
            if (head == NULL) {
                current = create;
                head = current;
                head->data = random() % 50;
            } else {
                current->next = create;
                current = create;
                current->data = random() % 50;
            }
        }
    }
    current->next = NULL;

    // count the number of elements in the list
    int count = 0;
    current = head;

    while (current != NULL) {
        count++;
        //printf("%i->", current->data);
        //if (current->next == NULL)
		//    puts("NULL");
        current = current->next;
    }

    // print the result
    printf("The number of elements in a linked list is %i\n", count);

    return(0);
}
