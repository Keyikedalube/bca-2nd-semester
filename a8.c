/*
 * Consider that a single linked list contains the following elements:
 * Roll_no. : integer
 * Name : string of maximum of 25 Character
 * Avg_no : float.
 * Write a program in C to represent a single linked list with the above elements.
 */

#include <stdio.h>
#include <stdlib.h>

typedef struct node_elements {
    int Roll_no;
    char Name[25];
    float Avg_no;
    struct node_elements *next;
} node;

node *head = NULL;
node *create = NULL;
node *current = NULL;

int no_of_students = 0;

void separator()
{
    puts("\n\n\n\n");
    for (int i = 40; i > 0; i--)
        printf("_");
    puts("\n\n\n\n");
}

void input_values()
{
    if (create == NULL) {
        puts("Memory allocation failed!");
        exit(1);
    } else {
        puts("");
        printf("Enter student name: ");
        scanf("%s", create->Name);
        printf("Enter student roll no: ");
        scanf("%i", &create->Roll_no);
        printf("Enter student avg no: ");
        scanf("%f", &create->Avg_no);
        puts("");
    }
}

void delete_list()
{
    if (head == NULL) {
        puts("No list exist! Create one first.");
    } else {
        current = head;
        while (current != NULL) {
            free(current);
            current = current->next;
        }
        head = current;
        puts("...List deleted");
    }
}

void create_nodes()
{
    if (head != NULL) {
        delete_list();
    }

input_again:
    printf("Enter the no of students: ");
    scanf("%i", &no_of_students);
    if (no_of_students <= 0) {
        puts("That's unrealistic! Input another number");
        goto input_again;
    }

    create = malloc(sizeof(node));
    head = create;
    current = head;
    for (int i = 1; i <= no_of_students; i++) {
        input_values();
        if (i == no_of_students) {
            break;
        }
        create = malloc(sizeof(node));
        current->next = create;
        current = create;
    }
    current->next = NULL;
}

void add_node()
{
    if (head == NULL) {
        puts("No list exist! Create one first.");
    } else {
input_again:
        printf("[Current students = %i] Enter position: ", no_of_students);
        unsigned int position = 0;
        scanf("%ui", &position);
        if (position < 1 || position > no_of_students+1) {
            puts("Input is not within the range! Try another number.");
            goto input_again;
        }

        // this node will hold previous current node
        node *temporary = NULL;
        no_of_students++;
        current = head;
        for (int i = 1; i <= no_of_students+1; i++) {
            if (position == 1) {
                create = malloc(sizeof(node));
                input_values();
                head = create;
                head->next = current;
                break;
            } else if (position == i) {
                create = malloc(sizeof(node));
                input_values();
                temporary->next = create;
                create->next = current;
                break;
            }
            temporary = current;
            current = current->next;
        }
    }
}

void delete_node()
{
    if (head == NULL) {
        puts("No list exist! Create one first.");
    } else {
input_again:
        printf("[Current students = %i] Enter position: ", no_of_students);
        unsigned int position = 0;
        scanf("%ui", &position);
        if (position < 1 || position > no_of_students) {
            puts("Input is not within the range! Try another number.");
            goto input_again;
        }

        // this node will hold previous current node
        node *temporary = NULL;
        current = head;
        for (int i = 1; i <= no_of_students; i++) {
            // delete first node
            if (position == 1) {
                free(current);
                head = current->next;
                break;
            } else if (position == i) {
                free(current);
                temporary->next = current->next;
                break;
            }
            temporary = current;
            current = current->next;
        }
        --no_of_students;
    }
}

void print_list()
{
    if (head == NULL) {
        puts("No list exist! Create one first.");
    } else {
        current = head;
        while (current != NULL) {
            printf("Roll no: %i\n", current->Roll_no);
            printf("Name: %s\n", current->Name);
            printf("Average no: %f\n\n", current->Avg_no);
            current = current->next;
        }
    }
}

int main()
{
    int option = 0;
    do {
        puts("\t1. Create list.");
        puts("\t2. Add node.");
        puts("\t3. Delete node.");
        puts("\t4. Delete list");
        puts("\t5. Print list.");
        puts("\t6. Exit");
        printf("Enter your option: ");
        scanf("%i", &option);

        switch (option) {
        case 1:
            separator();
            create_nodes();
            separator();
            break;
        case 2:
            separator();
            add_node();
            separator();
            break;
        case 3:
            separator();
            delete_node();
            separator();
            break;
        case 4:
            separator();
            delete_list();
            separator();
            break;
        case 5:
            separator();
            print_list();
            separator();
            break;
        case 6:
            // make sure list is cleared
            if (head != NULL) {
                delete_list();
            }
            break;
        default:
            separator();
            puts("Invalid choice! Try again");
            separator();
        }
    } while (option != 6);

    return(0);
}
