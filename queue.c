/*
 * Program to implement queue
 */

#include <stdio.h>
#include <stdlib.h>

typedef struct singly_linked {
        char data;
        struct singly_linked *next;
} queue;

queue *head     = NULL;
queue *current  = NULL;
queue *temp     = NULL;

int is_empty()
{
        if (head == NULL)
                return 1;
        else
                return 0;
}

void enqueue(char item)
{
        temp = malloc(sizeof(queue));
        if (temp == NULL) {
                puts("Memory allocation failed!");
                exit(1);
        }
        temp->data = item;

        if (is_empty()) {
                head = temp;
                current = temp;
        } else {
                current->next = temp;
                current = temp;
        }
        current->next = NULL;
}

void dequeue()
{
        if (!is_empty()) {
                puts("");
                printf("Dequeue %c\n", head->data);
                puts("");

                current = head->next;
                free(head);
                head = current;
        } else {
                puts("");
                puts("Queue's empty! Nothing to dequeue");
                puts("");
        }
}

char get_front()
{
        char item = '\0';

        if (!is_empty())
                item = head->data;

        return item;
}

int get_size()
{
        int size = 0;

        if (!is_empty()) {
                current = head;
                
                while (current != NULL) {
                        size++;
                        current = current->next;
                }

        }

        return size;
}

int main(int argc, char *argv[])
{
        if (argc == 2) {
                /* Data is already provided at command line */
                puts("Queue-ing data...");
                for (int i = 0; argv[1][i] != '\0'; i++) {
                        printf("%c\n", argv[1][i]);
                        enqueue(argv[1][i]);
                }
                puts("Data queued successfully!");
        }

        int option = 0;
        while (option != 6) {
                puts("Choose one of the following:");
                puts("\t1. enqueue item");
                puts("\t2. dequeue");
                puts("\t3. get front item");
                puts("\t4. is queue empty");
                puts("\t5. get queue size");
                puts("\t6. exit");
                printf("\tYour Choice: ");
                scanf("%i", &option);

                switch (option) {
                case 1:
                        puts("");
                        printf("Enter item to queue: ");
                        char item;
                        scanf(" %c", &item);
                        puts("");

                        enqueue(item);
                        break;
                case 2:
                        dequeue();
                        break;
                case 3:
                        puts("");
                        printf("The current front item is: %c", get_front());
                        puts("\n");
                        break;
                case 4:
                        puts("");
                        if (is_empty())
                                puts("The queue is empty");
                        else
                                puts("The queue is not empty");
                        puts("");
                        break;
                case 5:
                        puts("");
                        printf("The queue size is: %i", get_size());
                        puts("\n");
                        break;
                case 6:
                        /* while loop exit code */
                        break;
                default:
                        puts("Wrong option! Try again...");
                }
        }

        return 0;
}
