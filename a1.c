/*
 * An array A contains 25 positive integers. Write a program in C which will
 * find out the number of odd and even numbers in that array.
 */

#include <stdio.h>
#include <stdlib.h>

int main()
{
    int array[25];

    // store 25 random positive integer numbers in the array
    for (int i = 0; i < 25; i++) {
        array[i] = rand() % 50;
    }

    // print the array
    for (int i = 0; i < 25; i++) {
        printf("array[%i] = %i\n", i, array[i]);
    }
    puts("");

    // get odd and even numbers
    int no_odd = 0;
    int no_even = 0;

    for (int i = 0; i < 25; i++) {
        if (array[i] % 2 == 0) {
            no_even++;
        } else {
            no_odd++;
        }
    }

    printf("No of odd numbers: %i\n", no_odd);
    printf("No of even numbers: %i\n", no_even);

    return(0);
}
