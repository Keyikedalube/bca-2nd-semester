/*
 * Stack program: push and pop operation using doubly linked list
 */

#include <stdio.h>
#include <stdlib.h>

typedef struct stack {
    short datum;
    struct stack *next;
    struct stack *previous;
} node;

node *head = NULL;
node *temporary = NULL;
node *link = NULL;
node *last = NULL;

void separator()
{
    puts("");
    for (int i = 40; i > 0; i--)
        printf("_");
    puts("\n");
}

void push()
{
    temporary = malloc(sizeof(node));
    if (temporary == NULL) {
        printf("\tMemory allocation failed\n");
        exit(1);
    }
    
    // get datum value
    printf("\tEnter integer value (0-9): ");
    scanf("%hi", &temporary->datum);
    
    if (head == NULL) {
        // if push is done for the first time then
        head = temporary;
        head->previous = NULL;
        link = temporary;
    } else {
        // second or more push operation
        link->next = temporary;
        temporary->previous = link;
        link = temporary;
    }
    
    last = link;
    last->next = NULL;
}

void pop()
{
    if (head == NULL) {
        printf("\tNothing to pop\n");
    } else {
        link = link->previous;
        free(last);
        
        if (link != NULL) {
            // last (only) node precaution
            link->next = NULL;
        } else {
            head = link;
        }
        
        printf("\tLast value popped\n");
    }
    
	last = link;
}

void print()
{
    if (head == NULL) {
        printf("\tEmpty stack\n");
    } else {
        while (last != NULL) {
            puts("\t ---");
            printf("\t| %i |\n", last->datum);
            last = last->previous;
        }
        // decoration
        puts("\t\\___/");
    }
    
    last = link;
}

void clear_stack()
{
    while (last != NULL) {
        link = link->previous;
        free(last);
        
        if (link != NULL) {
            // last (only) node precaution
            link->next = NULL;
        } else {
            head = link;
            break;
        }
        
        last = link;
    }
}

int main()
{
    short choice;
    while (1) {
        puts("1\tPush");
        puts("2\tPop");
        puts("3\tPrint stack");
        puts("4\tExit");
        printf("Enter your choice: ");
        scanf("%hi", &choice);
        switch (choice) {
        case 1:
            separator();
            push();
            separator();
            break;
        case 2:
            separator();
            pop();
            separator();
            break;
        case 3:
            separator();
            print();
            separator();
            break;
        case 4:
            if (head != NULL) {
                clear_stack();
            }
            //print();
            return(0);
        default:
            separator();
            printf("\tInvalid choice!\n");
            separator();
        }
    }
}
