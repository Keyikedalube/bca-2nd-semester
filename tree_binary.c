/*
 * Program to implement binary tree with the following functions
 * Insert, search, & delete data
 * In-order, pre-order, & post-order traversal
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct node_properties {
        int data;
        struct node_properties *left;
        struct node_properties *right;
} binary;

binary *root    = NULL;
binary *parent  = NULL;
binary *current = NULL;

int temp_data = 0;


binary *new_node(int data)
{
        binary *node = (binary *)malloc(sizeof(binary));

        if (node == NULL) {
                puts("Memory allocation failed!");
                exit(1);
        }

        node->data = data;
        node->left = NULL;
        node->right = NULL;

        return node;
}


int is_tree_empty()
{
        int status = 0;

        if (root == NULL)
                status = 1;

        return status;
}


void insert_data(int data)
{
        binary *node = new_node(data);

        if (is_tree_empty()) {
                root = node;
                //printf("root");
        } else {
                parent = root;
                current = parent;

                while (1) {
                        if (data > parent->data) {
                                current = current->right;
                                if (current == NULL) {
                                        parent->right = node;
                                        //printf("->right");
                                        break;
                                } else
                                        parent = current;
                                //printf("->right");
                        } else if (data != parent->data) {
                                current = current->left;
                                if (current == NULL) {
                                        parent->left = node;
                                        //printf("->left");
                                        break;
                                } else
                                        parent = current;
                                //printf("->left");
                        } else {
                                puts("\nSorry, cannot insert duplicate data\n");
                                break;
                        }
                }
        }
}


int search_data(int data)
{
        int data_found = 0;

        if (!is_tree_empty()) {
                current = root;
                parent = NULL;

                while (!data_found) {
                        if (data > current->data) {
                                parent = current;
                                current = current->right;
                                if (current == NULL)
                                        break;
                        } else if (data < current->data) {
                                parent = current;
                                current = current->left;
                                if (current == NULL)
                                        break;
                        } else {
                                data_found = 1;
                                break;
                        }
                }
        } else
                puts("\nTree's empty!\n");

        return data_found;
}


void get_next_successor(binary *node)
{
        if (node->left != NULL) {
                node = node->left;
                temp_data = node->data;
                get_next_successor(node);
        }
}


void get_next_predecessor(binary *node)
{
        if (node->right != NULL) {
                node = node->right;
                temp_data = node->data;
                get_next_successor(node);
        }
}


void delete_data(int data)
{
        if (search_data(data)) {
                if (current->left == NULL && current->right == NULL) {
                        /* No child */

                        if (parent == NULL) {
                                /* We are deleting the root */
                                root = NULL;
                        } else {
                                if (parent->left->data == data)
                                        parent->left = NULL;
                                else
                                        parent->right = NULL;
                        }

                        free(current);

                } else if (current->left == NULL) {
                        /* A child on the right */

                        if (parent == NULL) {
                                /* We are deleting the root */
                                root = current->right;
                        } else {
                                if (parent->left->data == data)
                                        parent->left = current->right;
                                else
                                        parent->right = current->right;
                        }

                        free(current);

                } else if (current->right == NULL) {
                        /* A child on the left */

                        if (parent == NULL) {
                                /* We are deleting the root */
                                root = current->left;
                        } else {
                                if (parent->right->data == data)
                                        parent->right = current->left;
                                else
                                        parent->left = current->left;
                        }

                        free(current);
                } else {
                        /* Has two childs */

                        int is_root_being_deleted = 0;
                        if (parent == NULL) {
                                /* We are deleting the root */
                                is_root_being_deleted = 1;
                        }

                        if (current->right != NULL) {
                                temp_data = current->right->data;
                                get_next_successor(current->right);
                        } else {
                                temp_data = current->left->data;
                                get_next_predecessor(current->left);
                        }

                        /*
                         * Hold current node before deleting
                         * the successor/predecessor node
                         */
                        binary *temp_node = current;

                        delete_data(temp_data);

                        /*
                         * Restore and replace current node data with
                         * the successor/predecessor node
                         */
                        current = temp_node;
                        current->data = temp_data;
                        
                        if (is_root_being_deleted)
                                root = current;
                }
        }
}


void in_order(binary *node)
{
        if (node == NULL)
                return;

        in_order(node->left);

        printf("%d ", node->data);

        in_order(node->right);
}


void pre_order(binary *node)
{
        if (node == NULL)
                return;

        printf("%d ", node->data);

        pre_order(node->left);

        pre_order(node->right);
}


void post_order(binary *node)
{
        if (node == NULL)
                return;

        post_order(node->left);

        post_order(node->right);

        printf("%d ", node->data);
}


void delete_tree()
{
        while (root != NULL) {
                delete_data(root->data);

                //in_order(root);
                //puts("");
        }
        
        //printf("is tree empty? %s\n", is_tree_empty() ? "YES" : "NO");
}


int get_int_data()
{
        /*
         * From StackOverflow
         * Which is the best way to get input from user in C
         */
        char line[11];
        int data;
        int keep_looping = 1;

        while (keep_looping) {
                if (fgets(line, sizeof(line), stdin)) {
                        if (1 == sscanf(line, "%d", &data))
                                keep_looping = 0;
                }
        }

        return data;
}


int main(int argc, char *argv[])
{
        /* Input was supplied at command line */
        if (argc > 1) {
                puts("Inserting data:");
                for (int i = 1; i < argc; i++) {
                        printf("%s ", argv[i]);
                        int data = atoi(argv[i]);
                        insert_data(data);
                        puts("");
                }
                puts("");
        }

        int keep_looping = 1;
        while (keep_looping) {
                puts("Choose one of the following:");
                puts("\t1. Insert data");
                puts("\t2. Search data");
                puts("\t3. Delete data");
                puts("\t4. Print in-order");
                puts("\t5. Print pre-order");
                puts("\t6. Print post-order");
                puts("\t7. Exit program");
                printf("\tYour Choice: ");
                switch (get_int_data()) {
                       case 1:
                               printf("Enter data to insert: ");
                               insert_data(get_int_data());
                               break;
                       case 2:
                               printf("Enter data to search: ");
                               if (search_data(get_int_data()))
                                       puts("\nData found!");
                               else
                                       puts("\nData not found!");
                               puts("");
                               break;
                       case 3:
                               printf("Enter data to delete: ");
                               delete_data(get_int_data());
                               break;
                       case 4:
                               puts("");
                               in_order(root);
                               puts("\n");
                               break;
                       case 5:
                               puts("");
                               pre_order(root);
                               puts("\n");
                               break;
                               break;
                       case 6:
                               puts("");
                               post_order(root);
                               puts("\n");
                               break;
                       case 7:
                               keep_looping = 0;
                               delete_tree();
                               break;
                       default:
                               puts("");
                               puts("Invalid choice. Try again...");
                               puts("");
                }
        }

        return 0;
}
