#include <stdlib.h>
#include <stdio.h>

#define IS_LOG_MODE_ON 0

typedef struct node {
	char ch;
	int prc;
	struct node *next;
	struct node *prev;
} postfix;

postfix *head = NULL;
postfix *link = NULL;
postfix *temp = NULL;
postfix *last = NULL;

void print_stack()
{
	postfix *i = head;

	while (i != NULL) {
		printf("%c", i->ch);
		i = i->next;
	}
	puts("");
}

void pop()
{
	if (IS_LOG_MODE_ON) {
		printf("pop\t%c\t", last->ch);
		print_stack();
	} else {
		if (last->ch != '(')
			printf("%c", last->ch);
	}

	free(last);

	last = last->prev;
	if (last != NULL)
		last->next = NULL;
	else
		head = last;
	link = last;
}

void check_and_pop(int prc)
{
	short is_open_bracket_popped = 0;

	while (head != NULL && last->prc >= prc) {
		if (last->prc == prc && prc == 0)
			is_open_bracket_popped = 1;

		pop();

		if (is_open_bracket_popped)
			break;
	}
}

void push(char ch, int prc)
{
	if (prc != 0)
		check_and_pop(prc);

	temp = malloc(sizeof(postfix));

	if (temp == NULL) {
		puts("Memory allocation failed!");
		exit(1);
	}

	temp->ch = ch;
	temp->prc = prc;

	if (head == NULL) {
		head = temp;
		link = temp;
		head->prev = NULL;
	} else {
		temp->prev = link;
		link->next = temp;
		link = temp;
	}
	link->next = NULL;
	last = link;

	if (IS_LOG_MODE_ON) {
		printf("push\t%c\t", ch);
		print_stack();
	}
}

void postfix_notation(char *argv[])
{
	// Print the numbers first
	for (int i = 0; argv[1][i] != '\0'; i++) {
		int prc = 0;
		short is_prc_set = 0;
		switch (argv[1][i]) {
			/*
			 * Precedence:
			 * 3) ^
			 * 2) * /
			 * 1) + -
			 * 0) ( )
			 */
			case '^':
				prc = 3;
				is_prc_set = 1;
			case '*':
			case '/':
				if (!is_prc_set) {
					prc = 2;
					is_prc_set = 1;
				}
			case '+':
			case '-':
				if (!is_prc_set) {
					prc = 1;
					is_prc_set = 1;
				}
			case '(':
				if (!is_prc_set)
					prc = 0;
				push(argv[1][i], prc);
				break;
			case ')':
				if (!is_prc_set)
					prc = 0;
				check_and_pop(prc);
				break;
			default:
				if (IS_LOG_MODE_ON)
					printf("print\t%c\n", argv[1][i]);
				else
					printf("%c", argv[1][i]);
		}
	}

	// Print the remaining operators
	while (head != NULL)
		pop();

	puts("");
}

int main(int argc, char *argv[])
{
	if (argc == 2)
		postfix_notation(argv);
	else {
		puts("Usage:");
		puts("\tstack_ifx2pfx <expression>");
	}

	return(0);
}
