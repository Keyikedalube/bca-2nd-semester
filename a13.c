/*
 * Write a C program to implement doubly linked list.
 */

#include <stdio.h>
#include <stdlib.h>

typedef struct datum {
    int number;
    struct datum *next;
    struct datum *previous;
} node;

node *head = NULL;
node *current = NULL;
// used during insertion operation (aka 'previous')
node *temporary = NULL;
node *link = NULL;

int size = 0;

void separator()
{
    puts("\n\n\n\n");
    for (int i = 40; i > 0; i--)
        printf("_");
    puts("\n\n\n\n");
}

void input_datum(int i)
{
    if (current == NULL)
        printf("\tAllocation failed :(");
    else {
        printf("\tEnter data %i: ", i);
        scanf("%d", &(*current).number);
    }
}

void delete()
{
    if (head == NULL)
        printf("\tNo list to delete :(");
    else {
        for (int i = 1; i <= size; i++) {
	        link = head;
	        head = (*head).next;
            free(link);
            printf("\tNode %i deleted...\n", i);
            
            // deletion was done node after node
            // and now it has reached to null (assigned to temporary using next pointer)
            // Thus, breaking out of the loop to avoid using the previous pointer
            if (i == size)
                break;
        	
            (*head).previous = NULL;
        }
        size = 0;
    }
}

void create()
{
    if (head != NULL) {
        puts("Previous list not cleared!!! Clearing it now...");
        delete();
    }
    printf("\tEnter the size of nodes: ");
    scanf("%i", &size);
    // an exception to note down here...
    // node 1 is already created before for loop starts down there
    // so size is a lil' bit of a concern i.e, if size was 3 then
    // the actual loop needs to be over 2 but that's also an issue
    // because node 1 hasn't been inputted before for loop ever
    // executes. So as usual, loop over the actual size condition
    // but the allocation during the loop would be size-1
    current = malloc(sizeof(node));
    head = current;
    (*head).previous = NULL;
    link = head;
    for (int i = 1; i <= size; i++, link = current) {
        input_datum(i);
        if (i == size)
            break;
        current = malloc(sizeof(node));
        (*link).next = current;
        (*current).previous = link;
    }
    (*link).next = NULL;
}

void delete_nth()
{
    if (head == NULL)
        printf("\tNo list to delete :(");
    else {
	    printf("Enter the nth node to delete [1 - %i]: ", size);
        unsigned input;
        scanf("%ui", &input);
        if (input <= 0 || input > size)
            printf("\tOut of range!!!");
        else {
            link = head;
            for (int i = 1; i <= size; i++, link = (*link).next) {
                // first node is being deleted
                if (input == 1) {
                    head = (*head).next;
                    free(link);
                    printf("\tNode %i deleted...\n", i);
                    
                    // deletion was done node after node (maybe in random order)
                    // and now it has reached to null (assigned to head using next pointer)
                    // Thus, breaking out of the loop to avoid using the previous pointer
                    if (input == size)
                        break;
                    
                	(*head).previous = NULL;
                    break;
                }
                // node other than first is being deleted
                else if (input == i) {
		            current = link;
		            link = (*link).next;
		            free(current);
		            (*temporary).next = link;
		            
				    // last node precaution, making sure previous pointer is not used
		            if (input == size)
		                break;
		            
		            (*link).previous = temporary;
                    printf("\tNode %i deleted...\n", i);
                    break;
                }
                temporary = link;
            }
            --size;
        }
    }
}

void insert_nth()
{
    if (head == NULL)
        printf("\tNo list to insert :(");
    else {
        printf("\tEnter the nth node to insert into [1 - %i]: ", size+1);
        unsigned input;
        scanf("%ui", &input);
        if (input <= 0 || input > size+1)
            printf("\tOut of range!!!");
        else {
            size++;
            link = head;
            for (int i = 1; i <= size; i++, link = (*link).next) {
                // first node is being inserted
                if (input == 1) {
                    current = malloc(sizeof(node));
                    input_datum(i);
                    printf("\tNode %i inserted...", i);
                    head = current;
                    (*head).next = link;
                    (*link).previous = head;
                    (*head).previous = NULL;
                    break;
                }
                // node other than first is being inserted
                else if (input == i) {
                    current = malloc(sizeof(node));
                    input_datum(i);
                    printf("\tNode %i inserted...", i);
                    (*temporary).next = current;
                    (*current).previous = temporary;
                    (*current).next = link;
                    
                    // last node was appended, so avoid using previous pointer
                    // as null is not a node and has neither next nor previous
                    // pointer in it.
                    if (input == size)
                        break;

                    (*link).previous = current;
                    break;
                }
                temporary = link;
            }
        }
    }
}

void print()
{
    if (head == NULL)
        printf("\tNo list to print :(");
    else {
        // print forward using next pointer
        link = head;
        printf("\tNULL");

        while (link != NULL) {
        	printf("<-%d->", (*link).number);
            // store the last node
			if ((*link).next == NULL)
				current = link;
			link = (*link).next;
        }
        puts("NULL");
        
        // now print backward using previous pointer
		link = current;
        printf("\tNULL");
        while (link != NULL) {
	        printf("<-%d->", (*link).number);
	        link = (*link).previous;
        }
        printf("NULL");
    }
}

void terminate()
{
    if (head != NULL) {
        puts("List is not cleared from memory!!! Clearing it now...");
        delete();
    }
    puts("Exiting the program now");
}

int main()
{
    // char has the lowest size in C so use of int was ignored
    // y as in 'yes'
    // n as in 'no'
    char loop = 'y';
    do {
        puts("1\tCreate list");
        puts("2\tDelete nth node");
        puts("3\tDelete entire list");
        puts("4\tInsert nth node");
        puts("5\tPrint list");
        puts("6\tExit");
        printf("\nEnter choice of operation: ");
        short choice;
        scanf("%hi", &choice);
        // menu based program to create, delete, insert, etc the doubly linked list
        switch (choice) {
        case 1:
            separator();
            create();
            separator();
            break;
        case 2:
            separator();
            delete_nth();
            separator();
            break;
        case 3:
            separator();
            delete();
            separator();
            break;
        case 4:
            separator();
            insert_nth();
            separator();
            break;
        case 5:
            separator();
            print();
            separator();
            break;
        case 6:
            separator();
            terminate();
            loop = 'n';
            separator();
            break;
        default:
            separator();
            printf("Invalid option!");
            separator();
        }
    } while (loop == 'y');

    return(0);
}
