/*
 * Write a C program to find the location of the last node in a sorted linked list
 */

#include <stdio.h>
#include <stdlib.h>

typedef struct singly {
	int data;
	struct singly *next;
} list;

list *head = NULL;
list *node = NULL;
list *current = NULL;

int main()
{
	// Create a sorted list of random size
	int size = rand() % 100;
	for (short i = 1; i <= size; i++) {
		node = malloc(sizeof(list));
		if (node == NULL) {
			puts("Memory allocation failed");
			exit(1);
		}
		
		node->data = i;
		
		if (head == NULL) {
			head = node;
			head->next = NULL;
			current = head;
		} else {
			current->next = node;
			current = node;
		}
	}
	current->next = NULL;
	
	// print the list
	current = head;
	while (current != NULL) {
		printf("%i->", current->data);
		if (current->next == NULL)
			puts("NULL");
		current = current->next;
	}
	
	// find last node location
	int location = 0;
	current = head;
	while (current != NULL) {
		location += 1;
		current = current->next;
	}
	printf("The location of last node is %i\n", location);
	
	return(0);
}
