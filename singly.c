/*
 * Program demonstrating singly linked list
 */

#include <stdio.h>
#include <stdlib.h>

typedef struct datum {
    int number;
    struct datum *next;
} node;

node *head = NULL;
node *current = NULL;
node *temporary = NULL;
node *link = NULL;

int size = 0;

void separator()
{
    puts("\n\n\n\n");
    for (int i = 40; i > 0; i--)
        printf("_");
    puts("\n\n\n\n");
}

void input_datum(int i)
{
    if (current == NULL)
        printf("\tAllocation failed :(");
    else {
        printf("\tEnter data %i: ", i);
        scanf("%d", &(*current).number);
    }
}

void delete()
{
    if (head == NULL)
        printf("\tNo list to delete :(");
    else {
        for (int i = 1; i <= size; i++) {
		    current = head;
            head = (*head).next;
            free(current);
            printf("\tNode %i deleted...\n", i);
        }
        size = 0;
    }
}

void clear_previous_list()
{
    if (head != NULL) {
        puts("Previous list not cleared!!! Clearing it now...");
        delete();
    }
}

void create()
{
    clear_previous_list();
    printf("\tEnter the size of nodes: ");
    scanf("%i", &size);
    
    if (size != 0) {
        current = malloc(sizeof(node));
        head = current;
        // node 1 is already created before for loop executes
        // So the allocation during the loop would be size-1
        // unlike arrays, linked list can start indexing from 1 or 0
	link = head;
        for (int i = 1; i <= size; i++, link = current) {
            input_datum(i);
            
            if (i == size)
                break;
                
            current = malloc(sizeof(node));
            (*link).next = current;
        }
        // after all iterations it is important to tell the computer
        // that next is pointing to null
        (*link).next = NULL;
    }
}

void delete_nth()
{
    if (head == NULL)
        printf("\tNo list to delete :(");
    else {
        printf("Enter the nth node to delete [1 - %i]: ", size);
        unsigned input = 0;
        scanf("%ui", &input);
        
        // make sure user is not playing balls with the computer
        // by specifying input larger/smaller than the actual size!
        if (input <= 0 || input > size)
            printf("\tOut of range!!!");
        else {
            link = head;
            for (int i = 1; i <= size; i++, link = (*link).next) {
                if (input == 1) {
					// first node is being deleted
                    head = (*head).next;
                    free(link);
                    printf("\tNode %i deleted...\n", i);
                    break;
                }
                else if (input == i) {
					// node other than first is being deleted
					// this include middle node and last node too
                    (*current).next = (*link).next;
                    free(link);
                    printf("\tNode %i deleted...\n", i);
                    break;
                }
                // hold the current link before updating to next link
                // this way deleting a middle node or last node becomes easy
                current = link;
            }
            --size;
        }
    }
}

void insert_nth()
{
    if (head == NULL)
        printf("\tNo list to insert :(");
    else {
        printf("\tEnter the nth node to insert into [1 - %i]: ", size+1);
        unsigned input = 0;
        scanf("%ui", &input);
        
        // make sure input is within the bound of the list
        // and size+1 ensures that last node is appended
        if (input <= 0 || input > size+1)
            printf("\tOut of range!!!");
        else {
            // increment size by 1 as it's guaranteed to have a new node inserted
            size++;
            link = head;
            for (int i = 1; i <= size; i++, link = (*link).next) {
                if (input == 1) {
					// first node is being inserted
                    current = malloc(sizeof(node));
                    input_datum(i);
                    printf("\tNode %i inserted...", i);
                    head = current;
                    (*head).next = link;
                    break;
                }
                else if (input == i) {
					// node other than first is being inserted
					// this include appending to last node too
                    current = malloc(sizeof(node));
                    input_datum(i);
                    printf("\tNode %i inserted...", i);
                    (*temporary).next = current;
                    (*current).next = link;
                    break;
                }
                // hold the current link before updating to next link
                // this way inserting a middle node or last node becomes easy
                temporary = link;
            }
        }
    }
}

void print()
{
    if (head == NULL)
        printf("\tNo list to print :(");
    else {
        link = head;
        printf("\t");
        for (int i = 1; i <= size; i++, link = (*link).next)
            printf("%d->", (*link).number);
        printf("NULL");
    }
}

int main()
{
    // char has the lowest size in C so use of int was ignored
    // y as in 'yes'
    // n as in 'no'
    char loop = 'y';
    do {
        puts("1\tCreate list");
        puts("2\tDelete nth node");
        puts("3\tDelete entire list");
        puts("4\tInsert nth node");
        puts("5\tPrint list");
        puts("6\tExit");
        printf("\nEnter choice of operation: ");
        short choice = 0;
        scanf("%hi", &choice);
        switch (choice) {
        case 1:
            separator();
            create();
            separator();
            break;
        case 2:
            separator();
            delete_nth();
            separator();
            break;
        case 3:
            separator();
            delete();
            separator();
            break;
        case 4:
            separator();
            insert_nth();
            separator();
            break;
        case 5:
            separator();
            print();
            separator();
            break;
        case 6:
            separator();
            clear_previous_list();
    		puts("Exiting the program now");
            loop = 'n';
            separator();
            break;
        default:
            separator();
            printf("Invalid option!");
            separator();
        }
    } while (loop == 'y');

    return(0);
}
