/*
 * Write a program in C to implement stack using linked list.
 */

#include <stdio.h>
#include <stdlib.h>

// doubly linked list
typedef struct stack {
    short unsigned datum;
    struct stack *next;
    struct stack *previous;
} node;

node *head = NULL;
node *temporary = NULL;
node *link = NULL;
node *last = NULL;

void separator()
{
    puts("");
    for (int i = 40; i > 0; i--)
        printf("_");
    puts("\n");
}

void push()
{
    temporary = malloc(sizeof(node));
    if (temporary == NULL) {
        printf("\tMemory allocation failed\n");
        exit(1);
    }

    // get datum value
    do {
        printf("\tEnter integer value (0-9): ");
        scanf("%hu", &temporary->datum);
        if (temporary->datum < 0 || temporary->datum > 9) {
            puts("Input is not within the range! Try another number.");
        } else
            break;
    } while (1);

    if (head == NULL) {
        // if push is done for the first time then
        head = temporary;
        head->previous = NULL;
        link = temporary;
    } else {
        // second or more push operation
        link->next = temporary;
        temporary->previous = link;
        link = temporary;
    }
    last = link;
    link->next = NULL;
}

void pop()
{
    if (head == NULL) {
        printf("\tNothing to pop\n");
    } else {
        link = last;
        free(link);
        link = link->previous;
        if (link != NULL) {
            // last (only) node precaution
            link->next = NULL;
        } else {
            // otherwise head is assigned NULL
            head = link;
        }
        printf("\tLast value popped\n");
    }
    // last node update
    last = link;
}

void print()
{
    if (head == NULL) {
        printf("\tEmpty stack\n");
    } else {
        link = last;
        while (link != NULL) {
            puts("\t ---");
            printf("\t| %i |\n", link->datum);
            link = link->previous;
        }
        // decoration
        puts("\t\\___/");
    }
    // reset link to last node
    link = last;
}

void clear_stack()
{
    while (last != NULL) {
        free(last);
        last = last->previous;
        if (last != NULL) {
            // last (only) node precaution
            last->next = NULL;
        } else {
            // otherwise head is assigned NULL
            head = last;
            break;
        }
    }
}

int main()
{
    short choice;
    while (1) {
        puts("1\tPush");
        puts("2\tPop");
        puts("3\tPrint stack");
        puts("4\tExit");
        printf("Enter your choice: ");
        scanf("%hi", &choice);
        switch (choice) {
        case 1:
            separator();
            push();
            separator();
            break;
        case 2:
            separator();
            pop();
            separator();
            break;
        case 3:
            separator();
            print();
            separator();
            break;
        case 4:
            if (head != NULL) {
                clear_stack();
            }
            return(0);
        default:
            separator();
            printf("\tInvalid choice!\n");
            separator();
        }
    }
}
