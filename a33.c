/*
 * Write a program in C to implement Merge sort.
 */

#include <stdio.h>

void print(int array[], int size)
{
    for (int index = 0; index < size; index++) {
        printf("%i ", array[index]);
    }
    puts("");
}

int main()
{
    // let's just assume arrays are sorted in ascending
    int array1[] = {1, 2, 3, 4};
    int array2[] = {0, 5};
    
    // compute the two size
    int array1_size = sizeof(array1) / sizeof(array1[0]);
    int array2_size = sizeof(array2) / sizeof(array2[0]);
    
    // third array for storing merged arrays
    int array3_size = array1_size + array2_size;
    int array3[array3_size];
    
    int index1 = 0;
    int index2 = 0;
    int index3 = 0;

    puts("Elements of array1 is:");
    print(array1, array1_size);
    puts("Elements of array2 is:");
    print(array2, array2_size);

    // merge sorting algorithm
    while (index1 < array1_size && index2 < array2_size) {
        if (array1[index1] < array2[index2]) {
            array3[index3] = array1[index1];
            index3++;
            index1++;
        } else {
            array3[index3] = array2[index2];
            index3++;
            index2++;
        }
    }

    // copy the rest of the unmerged array1 elements
    while (index1 < array1_size) {
        array3[index3] = array1[index1];
        index3++;
        index1++;
    }

    // copy the rest of the unmerged array2 elements
    while (index2 < array2_size) {
        array3[index3] = array2[index2];
        index3++;
        index2++;
    }

    puts("Elements of merged array3 is:");
    print(array3, array3_size);

    return(0);
}
