/*
 * Write a program in C to implement Bubble Sort.
 */

#include <stdio.h>
#include <stdlib.h>

#define SIZE 10

void print(int array[])
{
    for (int i = 0; i < SIZE; i++) {
        printf("%i ", array[i]);
    }
    puts("");
}

void bubble_ascending(int array[])
{
    int i = 0;
    int flag = 0;

    while (i < SIZE-1 && !flag) {
        flag = 1;
        for (int j = 0; j < SIZE-1; j++) {
            if (array[j] > array[j+1]) {
                int temporary = array[j];
                array[j] = array[j+1];
                array[j+1] = temporary;
                flag = 0;
            }
        }
        i++;
    }
}

void bubble_descending(int array[])
{
    int i = 0;
    int flag = 0;

    while (i < SIZE-1 && !flag) {
        flag = 1;
        for (int j = 0; j < SIZE-1; j++) {
            if (array[j] < array[j+1]) {
                int temporary = array[j];
                array[j] = array[j+1];
                array[j+1] = temporary;
                flag = 0;
            }
        }
        i++;
    }
}

int main()
{
    int array[SIZE];

    // store random numbers in array
    for (int i = 0; i < SIZE; i++) {
        array[i] = rand() % 10;
    }

    puts("Before sorting:");
    print(array);

    bubble_ascending(array);
    puts("After sorting in ascending order:");
    print(array);

    bubble_descending(array);
    puts("After sorting in descending order:");
    print(array);

    return(0);
}
