/*
 * Write a program in C to implement Binary search
 */

#include <stdio.h>
#include <stdlib.h>

#define SIZE 50

void print(int array[])
{
	puts("Printing the array");
	for (short i = 0; i < SIZE; i++)
		printf("%i ", array[i]);
	puts("");
}

void selection(int array[])
{
	for (int i = 0; i < SIZE-1; i++) {
		int smallest = array[i];
		int j = i;
		// find the smallest
		for (int k = i+1; k < SIZE; k++) {
			if (array[k] < smallest) {
				smallest = array[k];
				j = k;
			}
		}
		// swap the smallest
		int temporary = array[i];
		array[i] = array[j];
		array[j] = temporary;
	}
}

int binary(int search, int array[])
{
	int begin = 0;
	int end = SIZE - 1;
	
	while (begin <= end) {
		int middle = (begin+end) / 2;
		
		if (array[middle] == search)
			return(middle);
		else {
			if (array[middle] < search)
				begin = middle + 1;
			else
				end = middle - 1;
		}
	}
	
	return(-1);
}

int main()
{
	int array[SIZE];
	
	for (short i = 0; i < SIZE; i++)
		array[i] = rand() % 100;
		
	//print(array);
	selection(array);
	print(array);
	
	printf("Enter the value to search: ");
	int search;
	scanf("%i", &search);
	
	int position = binary(search, array);
	if (position != -1)
		printf("The position of %i was found at %i\n", search, position);
	else
		printf("The position of %i was not found\n", search);
	
	return(0);
}
