/*
 * Write a program in C to insert an element in the kth position of an array size 20.
 */

#include <stdio.h>
#include <stdlib.h>

void print(int array[], int array_size)
{
    for (int i = 0; i < array_size; i++) {
        printf("%i ", array[i]);
    }
    puts("");
}

int main()
{
    int array[] = {
        1, 12, 3, 14, 5, 16, 7, 18, 9, 10,
        11, 2, 13, 4, 15, 6, 17, 8, 19, 0
    };
    int array_size = sizeof(array) / sizeof(array[0]);

    // print array
    print(array, array_size);

    // get kth position
    printf("Enter the kth position: ");
    int kth = 0;
    scanf("%i", &kth);

    // get kth element
    printf("Enter element for %ith position: ", kth);
    scanf("%i", &array[kth]);

    // print array again
    print(array, array_size);

    return(0);
}
