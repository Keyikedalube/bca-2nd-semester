/*
 * Write a program in C to implement Linear search
 */

#include <stdio.h>
#include <stdlib.h>

const int SIZE = 10;

int linear(int search, int array[])
{
	for (short i = 0; i < SIZE; i++) {
		if (array[i] == search)
			return(i);
	}
	return(-1);
}

int main()
{
	int array[SIZE];
	
	for (short i = 0; i < SIZE; i++)
		array[i] = rand() % 50;
	
	int search = rand() % 50;
	int position = linear(search, array);
	
	/*
	for (short i = 0; i < SIZE; i++)
		printf("%i ", array[i]);
	puts("");
	*/
	
	if (position != -1)
		printf("The position of %i was found at %i\n", search, position);
	else
		printf("The position of %i was not found\n", search);
	
	return(0);
}
