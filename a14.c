/*
 * Write a program in C to implement stack using array
 */

#include <stdio.h>

#define SIZE 3

int stack[SIZE];

// count is updated only when push option is selected by the user. Hence, the initial value -1
// So the first time the user chooses push option, count is incremented to 0 and array value is inserted
// This makes sure the current count still points to the same array value in case the user
// decides to start popping midway before filling up the stack fully.
int count = -1;

void separator()
{
    puts("");
    for (int i = 40; i > 0; i--)
        printf("_");
    puts("\n");
}

void push()
{
    if (count < SIZE) {
        printf("\tEnter the value to push: ");
        scanf("%d", &stack[count]);
    } else
        printf("\tStack full!");
}

void pop()
{
    // this is to avoid repercussion during full push operation
    // count has reached 2 but has been incremented further to 3
    // because of the post increment in stack[count++]
    if (count == SIZE)
        count--;
    if (count != -1) {
        printf("\tPopping the last value...\n");
        stack[count--] = 0;
        printf("\tPopped!");
    } else
        printf("\tEmpty stack!\n");
}

void print()
{
    for (int i = SIZE-1; i >= 0; i--)
        printf("\t%d\n", stack[i]);
}

int main()
{
    do {
        printf("1\tPush on stack\n");
        printf("2\tPop from stack\n");
        printf("3\tPrint current stack\n");
        printf("4\tExit program\n");
        printf("\nEnter choice of operation: ");
        short unsigned choice;
        scanf("%hu", &choice);

        switch (choice) {
        case 1:
            separator();
            count++;
            push();
            separator();
            break;
        case 2:
            separator();
            pop();
            separator();
            break;
        case 3:
            separator();
            print();
            separator();
            break;
        case 4:
            return(0);
        default:
            separator();
            printf("\tInvalid choice!\n");
            separator();
        }
    } while (1);
}

