/*
 * Write a C program for multiplication of two spare matrix
 */

#include <stdio.h>
#include <stdlib.h>

#define SIZE 3

void set_values(int matrix[SIZE][SIZE])
{
	for (short i = 0; i < SIZE; i++)
		for (short j = 0; j < SIZE; j++)
			matrix[i][j] = rand() % 10;
}

void print(int matrix[SIZE][SIZE])
{
	for (short i = 0; i < SIZE; i++) {
		for (short j = 0; j < SIZE; j++)
			printf("%i ", matrix[i][j]);
		puts("");
	}
	puts("");
}

void multiply(int matrix_1[SIZE][SIZE], int matrix_2[SIZE][SIZE]) {
	int matrix_3[SIZE][SIZE];
	for (short i = 0; i < SIZE; i++) {
		for (short j = 0; j < SIZE; j++) {
			matrix_3[i][j] = 0;
			for (short k = 0; k < SIZE; k++)
				matrix_3[i][j] += matrix_1[i][k] * matrix_2[k][j];
		}
	}
	print(matrix_3);
}

int main()
{
	int matrix_1[SIZE][SIZE];
	int matrix_2[SIZE][SIZE];
	
	set_values(matrix_1);
	set_values(matrix_2);
	
	puts("Printing matrix 1");
	print(matrix_1);
	puts("Printing matrix 2");
	print(matrix_2);
	
	puts("Multiplication of matrix 1 and matrix 2");
	multiply(matrix_1, matrix_2);
	
	return(0);
}
