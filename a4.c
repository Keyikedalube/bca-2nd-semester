/*
 * Consider two single dimensional arrays of size 20 and 30 respectively.
 * Write a program in C to find out the elements which are common in both arrays.
 */

#include <stdio.h>
#include <stdlib.h>

#define ARRAY1_SIZE 20
#define ARRAY2_SIZE 30

void print(int *array, int array_size)
{
    for (int i = 0; i < array_size; i++) {
        printf("%i ", array[i]);
    }
    puts("");
}

int main()
{
    int array1[ARRAY1_SIZE];
    int array2[ARRAY2_SIZE];

    // store random numbers in array1
    for (int i = 0; i < ARRAY1_SIZE; i++) {
        array1[i] = random() % 10;
    }
    // store random numbers in array2
    for (int i = 0; i < ARRAY2_SIZE; i++) {
        array2[i] = random() % 20;
    }

    // print both arrays
    print(array1, ARRAY1_SIZE);
    print(array2, ARRAY2_SIZE);

    // sort array1 in ascending order using bubble sort
    int i = 0;
    int flag = 0;
    while (i++ < ARRAY1_SIZE-1 && !flag) {
        flag = 1;
        for (int j = 0; j < ARRAY1_SIZE-1; j++) {
            if (array1[j] > array1[j+1]) {
                int temporary = array1[j];
                array1[j] = array1[j+1];
                array1[j+1] = temporary;
                flag = 0;
            }
        }
    }

    // print common elements in both arrays
    int previous = 0;   // will store previous common element

    puts("\nCommon elements are:");
    for (int i = 0; i < ARRAY1_SIZE; i++) {
        for (int j = 0; j < ARRAY2_SIZE; j++) {
            if (array1[i] == array2[j] && array1[i] != previous) {
                printf("%i ", array1[i]);
                previous = array1[i];
            }
        }
    }
    puts("");

    return(0);
}
