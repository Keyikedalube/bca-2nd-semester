/*
 * Write a program in C to delete duplicate elements from an array of 20 integers.
 */

#include <stdio.h>
#include <stdlib.h>

#define ARRAY_SIZE 20

void print(int array[], int size)
{
    puts("The elements of array are:");
    for (int index = 0; index < size; index++)
        printf("%i ", array[index]);
    puts("\n");
}

void bubble_ascending(int array[], int size)
{
    int index1 = 0;
    int flag = 0;

    while (index1++ < size-1 && !flag) {
        flag = 1;
        for (int index2 = 0; index2 < size-1; index2++) {
            if (array[index2] > array[index2+1]) {
                int temporary = array[index2];
                array[index2] = array[index2+1];
                array[index2+1] = temporary;
                flag = 0;
            }
        }
    }
}

void delete_duplicate(int array[])
{
	// Since we're using last index value to replace the duplicate element
	// new_size actually needs to hold the last index ie, ARRAY_SIZE - 1
    int new_size = ARRAY_SIZE - 1;
    
    for (int index1 = 0; index1 < new_size; index1++) {
	    int index2 = index1 + 1;
	    //printf("Comparing %i and %i\n", array[index1], array[index2]);
	    //printf("\tindex1: %i\n", index1);
	    while (index2 <= new_size) {
		    //printf("\tindex2: %i\n", index2);
		    if (array[index1] == array[index2]) {
			    if (array[index2] != array[new_size])
				    array[index2] = array[new_size];
			    --new_size;
			    //print(array, new_size+1);
				continue;
		    }
		    index2++;
	    }
    }
    
    // print & bubble sort functions compare against the array size as n and not as n-1
	// which we have initialized with our new_size value at the top of this function.
	// Bringing it back from n-1 to n so the other two functions work with the complete
	// array set.
    new_size++;
    puts("\tAFTER DELETING");
    print(array, new_size);
    
    puts("=========================");
    puts("\tAFTER SORTING");
    bubble_ascending(array, new_size);
    print(array, new_size);
    puts("=========================");
}

int main()
{
    int array[ARRAY_SIZE];

    // store random numbers in array
    for (int index = 0; index < ARRAY_SIZE; index++)
        array[index] = rand() % 10;

    bubble_ascending(array, ARRAY_SIZE);
    puts("\tBEFORE DELETING");
    print(array, ARRAY_SIZE);

    delete_duplicate(array);
    
    return(0);
}
