/*
 * Write a program to convert a given decimal number to its binary form and store it in a stack.
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

typedef struct stack {
	int datum;
	struct stack *next;
	struct stack *prev;
} binary;

binary *head = NULL;
binary *temporary = NULL;
binary *link = NULL;
binary *last = NULL;

void push(int bit)
{
	temporary = malloc(sizeof(binary));
	if (temporary == NULL) {
		puts("Memory allocation failed!");
		exit(1);
	} else
		temporary->datum = bit;

	if (head == NULL) {
		head = temporary;
		head->prev = NULL;
		link = temporary;
	} else {
		link->next = temporary;
		temporary->prev = link;
		link = temporary;
	}
	link->next = NULL;
	last = link;
}

void pop()
{
	printf("%i", last->datum);

	free(last);

	last = last->prev;
	if (last != NULL)
		last->next = NULL;
	else
		head = last;
}

void decimal_to_binary()
{
	// get decimal
	printf("Enter decimal number: ");
	int decimal = 0;
	scanf("%i", &decimal);

	// convert to binary
	while (decimal != 0) {
		if (decimal % 2 != 0)
			push(1);
		else
			push(0);

		decimal /= 2;
	}
}

void print_binary()
{
	printf("Binary equivalent is: ");
	while (head != NULL) {
		pop();
	}
	puts("");
}

int main()
{
	decimal_to_binary();
	print_binary();

	return(0);
}
