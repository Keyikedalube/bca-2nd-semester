/*
 * Write a program in C to implement Selection sort.
*/

#include <stdio.h>
#include <stdlib.h>

#define SIZE 10

void print_array(int *array)
{
    for (int i = 0; i < SIZE; i++) {
        printf("%i ", array[i]);
    }
    puts("");
}

int main()
{
    int array[SIZE];

    // store random numbers in array
    for (int i = 0; i < SIZE; i++) {
        array[i] = random() % 10;
    }

    puts("Original array before sorting:");
    print_array(array);

    // selection sort algorithm: ascending
    for (int i = 0; i < SIZE-1; i++) {
        int smallest = array[i];
        int j = i;
        // find the smallest
        for (int k = i+1; k < SIZE; k++) {
            if (array[k] < smallest) {
                smallest = array[k];
                j = k;
            }
        }
        // swap the smallest
        int temporary = array[i];
        array[i] = array[j];
        array[j] = temporary;
    }

    puts("After sorting:");
    print_array(array);

    return(0);
}
