/*
 * Write a C program to insert an ITEM after a given node in a Linked list
 */

#include <stdio.h>
#include <stdlib.h>

typedef struct singly {
	int data;
	struct singly *next;
} list;

list *head;
list *node;
list *current;

void print()
{
	current = head;
	while (current != NULL) {
		printf("%i->", current->data);
		if (current->next == NULL)
			puts("NULL");
		current = current->next;
	}
}
	

int main()
{
	int size = rand() % 10;
	for (short i = 1; i <= size; i++) {
		node = malloc(sizeof(list));
		if (node == NULL) {
			puts("Node allocation failed!");
			exit(1);
		}
		
		node->data = rand() % 100;
		
		if (head == NULL) {
			head = node;
			head->next = NULL;
			current = head;
		} else {
			current->next = node;
			current = node;
		}
	}
	current->next = NULL;
	
	print();
	
	// get nth position
	int position = 0;
	while (1) {
		printf("Enter nth position to insert an item [1 - %i]: ", size+1);
		scanf("%i", &position);
		if (position < 1 || position > size+1)
			puts("Your position value exceeds the limit! Try again...");
		else
			break;
	}
	
	// get item value
	current = head;
	list *previous;
	for (short i = 1; i <= size+1; i++) {
		if (position == 1) {
			node = malloc(sizeof(list));
			printf("Enter item value: ");
			scanf("%i", &node->data);
			head = node;
			head->next = current;
			break;
		} else if (position == i) {
			node = malloc(sizeof(list));
			printf("Enter item value: ");
			scanf("%i", &node->data);
			previous->next = node;
			node->next = current;
			break;
		}
		previous = current;
		current = current->next;
	}			
	
	print();
	
	return(0);
}
