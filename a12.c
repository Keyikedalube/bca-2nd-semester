/*
 * Write a C program to implement circular linked list
 */

#include <stdio.h>
#include <stdlib.h>

typedef struct datum {
    int number;
    struct datum *next;
} node;

node *head = NULL;
node *current = NULL;
node *temporary = NULL;
node *link = NULL;

int size = 0;

void separator()
{
    puts("\n\n\n\n");
    for (int i = 40; i > 0; i--)
        printf("_");
    puts("\n\n\n\n");
}

void input_datum(int i)
{
    if (current == NULL)
        printf("\tAllocation failed :(");
    else {
        printf("\tEnter data %i: ", i);
        scanf("%d", &(*current).number);
    }
}

void delete()
{
    if (head == NULL)
        printf("\tNo list to delete :(");
    else {
        link = head;
        for (int i = 1; i <= size; i++) {
	        // notify node has been deleted
			current = link;
            link = (*link).next;
            free(current);
            printf("\tNode %i deleted...\n", i);
        }
        // assign null to head so the computer knows head is back
        // to nothingness again
        head = NULL;
    }
}

void create()
{
    if (head != NULL) {
        puts("Previous list not cleared!!! Clearing it now...");
        delete();
    }
    
    printf("\tEnter the size of nodes: ");
    scanf("%i", &size);
    
    // node 1 is already created (but not assigned) before for loop starts...
    current = malloc(sizeof(node));
    head = current;
	link = head;
    for (int i = 1; i <= size; i++, link = current) {
        input_datum(i);
        
        if (i == size)
            break;
            
        current = malloc(sizeof(node));
        (*link).next = current;
    }
    // assign head to next pointer of last node
    (*link).next = head;
}

void delete_nth()
{
    if (head == NULL)
        printf("\tNo list to delete :(");
    else {
        printf("Enter the nth node to delete [1 - %i]: ", size);
        unsigned input;
        scanf("%ui", &input);
        
        if (input <= 0 || input > size)
            printf("\tOut of range!!!");
        else {
            link = head;
            for (int i = 1; i <= size; i++, link = (*link).next) {
                // first node is being deleted
                if (input == 1) {
                    head = (*head).next;
                    free(link);
                    printf("\tNode %i deleted...\n", i);
                    break;
                }
                // node other than first is being deleted
                // this include middle node and last node too
                else if (input == i) {
	                temporary = link;
                    (*current).next = (*link).next;
                    free(temporary);
                    printf("\tNode %i deleted...\n", i);
                    break;
                }
                // hold the current link before updating to next link
                // this way deleting a middle node or last node becomes easy
                current = link;
            }
            if (--size == 0)
                head = NULL;
        }
    }
}

void insert_nth()
{
    if (head == NULL)
        printf("\tNo list to insert :(");
    else {
        printf("\tEnter the nth node to insert into [1 - %i]: ", size+1);
        unsigned input;
        scanf("%ui", &input);
        
        if (input <= 0 || input > size+1)
            printf("\tOut of range!!!");
        else {
            // increment size by 1 as it's guaranteed to have a new node inserted
            size++;
            link = head;
            for (int i = 1; i <= size; i++, link = (*link).next) {
                // first node is being inserted
                if (input == 1) {
                    current = malloc(sizeof(node));
                    input_datum(i);
                    printf("\tNode %i inserted...", i);
                    head = current;
                    (*head).next = link;
                    break;
                }
                // node other than first is being inserted
                // this include appending to last node too
                else if (input == i) {
                    current = malloc(sizeof(node));
                    input_datum(i);
                    printf("\tNode %i inserted...", i);
                    (*temporary).next = current;
                    (*current).next = link;
                    break;
                }
                temporary = link;
            }
        }
    }
}

void print()
{
    if (head == NULL)
        printf("\tNo list to print :(");
    else {
        link = head;
        printf("\t");
        for (int i = 1; i <= size; i++, link = (*link).next)
            printf("%d->", (*link).number);
        printf("head");
    }
}

void terminate()
{
    if (head != NULL) {
        puts("List is not cleared from memory!!! Clearing it now...");
        delete();
    }
    puts("Exiting the program now");
}

int main()
{
    // char has the lowest size in C so use of int was ignored
    // y as in 'yes'
    // n as in 'no'
    char loop = 'y';
    do {
        puts("1\tCreate list");
        puts("2\tDelete nth node");
        puts("3\tDelete entire list");
        puts("4\tInsert nth node");
        puts("5\tPrint list");
        puts("6\tExit");
        printf("\nEnter choice of operation: ");
        short choice;
        scanf("%hi", &choice);
        // menu based program to create, delete, insert, etc the list
        switch (choice) {
        case 1:
            separator();
            create();
            separator();
            break;
        case 2:
            separator();
            delete_nth();
            separator();
            break;
        case 3:
            separator();
            delete();
            separator();
            break;
        case 4:
            separator();
            insert_nth();
            separator();
            break;
        case 5:
            separator();
            print();
            separator();
            break;
        case 6:
            separator();
            terminate();
            loop = 'n';
            separator();
            break;
        default:
            separator();
            printf("Invalid option!");
            separator();
        }
    } while (loop == 'y');

    return(0);
}
