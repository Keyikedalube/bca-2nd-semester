/*
 * Write a program in C for traversing a Linear Array with a lower bound and upper bound.
 */

#include <stdio.h>
#include <stdlib.h>

const int SIZE = 10;

void assign(int array[])
{
    for (int index = 0; index < SIZE; index++)
        array[index] = rand() % 10;
}

void print(int array[], int lower_bound, int upper_bound)
{
    puts("The current array elements are:");
    while (lower_bound < upper_bound) {
        printf("%i ", array[lower_bound]);
        lower_bound++;
    }
    puts("");
}

int get_bound(char *string) {
    int bound;
    while (1) {
        printf("Enter %s bound value (0-%i): ", string, SIZE);
        scanf("%i", &bound);
        if (bound < 0 || bound > SIZE)
            puts("The specified bound exceeds the limit!");
        else
            break;
    }
    return(bound);
}

int main()
{
    int array[SIZE];
    int lower_bound = 0;
    int upper_bound = SIZE;

    assign(array);
    print(array, lower_bound, upper_bound);

    // get lower and upper bound
    lower_bound = get_bound("lower");
    upper_bound = get_bound("upper");

    print(array, lower_bound, upper_bound);

    return(0);
}
